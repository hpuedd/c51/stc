/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed         |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_delay.h"
#include "drv_lcd1602.h"
#include <intrins.h>
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define  LCD_RS  P20 
#define  LCD_WR  P21
#define  LCD_EN  P12 
#define  DATA    P0
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/


/**
  * @brief wait lcd1602 free
  * @param roll[in]: none
  * @return none
  */
static void _lcd_wait_ready(void)
{
    uint8_t sta;

    DATA = 0xff;
    P11 = 1;
    LCD_WR = 1;
    LCD_RS = 0;
    do
    {
        P11 = 1;
        LCD_EN = 1;
        sta = DATA;
        _nop_();_nop_();_nop_();
        LCD_EN = 0;
        P11 = 0;
    }while(sta & 0x80);    
}

static void _lcd_write_cmd(uint8_t cmd)
{
    uint8_t tmp_P2, tmp_P0;
    /* save p2 and p0 status */
    tmp_P2 = P2;
    tmp_P0 = P0;
    /* pull up to wait ready */
    P2 = 0xff;             
    P0 = 0xff;
    delay_ms(1);
    _lcd_wait_ready();

    P11 = 1; 	
    LCD_RS = 0;
    LCD_WR = 0;
    DATA = cmd;
    delay_us(1);            
    LCD_EN = 1;
    LCD_EN = 0;
    P11 = 0;
    /* pop p2 dan p0 status */
    P2 = tmp_P2;
    P0 = tmp_P0;           
    delay_us(20);
}

static void _lcd_write_data(uint8_t dat)
{
    uint8_t tmp_P2, tmp_P0;
    /* save p2 and p0 status */
    tmp_P2 = P2;
    tmp_P0 = P0;
    /* pull up to wait ready */
    P2 = 0xff;
    P0 = 0xff;
    delay_ms(1);
    _lcd_wait_ready();

    P11 = 1;
    LCD_RS = 1;
    LCD_WR = 0;
    DATA = dat;
    delay_us(1);
    LCD_EN = 1;
    LCD_EN = 0;
    P11 = 0;

    /* pop p2 dan p0 status */
    P2 = tmp_P2;
    P0 = tmp_P0;           
    delay_us(20);
}

/**
  * @brief lcd1602 show string
  * @param roll[in]: x Horizontal coordinates <16
  * @param roll[in]: y along slope coordinate <2
  * @param roll[in]: str string
  * @return none
  */
void lcd_showString(uint8_t x, uint8_t y, uint8_t *str)
{
    /* confirm position */
    _lcd_write_cmd((((y & 1) << 6) + x) | 0x80);
    /* cycle writedata */
    while(*str)
        _lcd_write_data(*str++);
}

/**
  * @brief lcd1602 init
  * @param roll[in]: none
  * @return none
  */
void lcd_init(void)
{
    delay_ms(15);
    _lcd_write_cmd(0x38);
    delay_ms(5);
    _lcd_write_cmd(0x38);
    delay_ms(5);
    _lcd_write_cmd(0x38);
    _lcd_write_cmd(0x38);
    _lcd_write_cmd(0x08);
    _lcd_write_cmd(0x01);
    _lcd_write_cmd(0x06);
    _lcd_write_cmd(0x0c);
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
