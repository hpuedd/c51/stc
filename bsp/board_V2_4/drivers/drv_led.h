/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_uart.h
  * @version 0.1.0
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-21   |   WS          |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_LED_H__
#define __DRV_LED_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
extern uint8_t P0_BUFF;///<Save lamp status, compatible with digitron scanning
/* Exported macro --------------------------------------------------------------------------*/
typedef enum 
{
    LED0 = 0,
    LED1,
    LED2,
    LED3,
    LED4,
    LED5,
    LED6,
    LED7,
    LED_MAX,
} led_list_t;

typedef enum 
{
    LED_ON  = 0,
    LED_OFF = 1,
} led_status_t;
/* Exported functions ----------------------------------------------------------------------*/
void led_run(void);
void led_switch(led_list_t ledn,led_status_t status);
#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
