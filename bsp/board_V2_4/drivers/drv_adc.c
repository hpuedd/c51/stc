/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed       |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_adc.h"
#include "drv_delay.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define ADC_POWER 0x80 
#define ADC_FLAG 0x10 
#define ADC_START 0x08 
#define ADC_SPEEDLL 0x00 //540 clocks
#define ADC_SPEEDL 0x20 //360 clocks
#define ADC_SPEEDH 0x40 //180 clocks
#define ADC_SPEEDHH 0x60
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

void adc_init(void)
{
    P1ASF |= 0X01;		  	///<config P10 function to A/D 
    ADC_RES = 0;         	///<clear data output register
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL;
    delay_ms(5);
}

/**
  * @brief get adc change value
  * @param roll[in]: ch "channel" (0x00~0x07)
  * @return adc value
  */
uint8_t get_adcResult(uint8_t ch)
{
    /* ADRJ = 0 8 bit change*/
    ADC_CONTR = ADC_POWER|ADC_SPEEDLL|ADC_START|ch;   
    delay_ms(5);
    /* change over when ADC_FLAG == 1 */
    while(!(ADC_CONTR&ADC_FLAG));    
    ADC_CONTR &= ~ADC_FLAG;
    
    return ADC_RES;
}

/**
  * @brief change ADC data to float
  * @param roll[in]: ch "channel" (0x00~0x07)
  * @return voltage (0~255)
  */
uint8_t get_voltage(uint8_t ch)
{
    uint8_t vol;
    
    vol = get_adcResult(ch);
    
    return vol;
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
