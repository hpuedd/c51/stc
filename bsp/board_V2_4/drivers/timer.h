/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file timer.h
  * @version 0.1.0	
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-18   |   WS          |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __TIMER_H__
#define __TIMER_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/   
#include "board.h" 
#include "drv_digitron.h"    
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
extern bit digitron_scanFlag;
extern bit led_runFlag;
/* Exported macro --------------------------------------------------------------------------*/ 	
/* Exported functions ----------------------------------------------------------------------*/  
void timer0_init(void);
#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
