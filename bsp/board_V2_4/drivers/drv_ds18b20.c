/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed       |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_ds18b20.h"
#include "drv_delay.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define PIN_18B20 P35
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

/**
  * @brief get ds18b20 action
  * @param roll[in]: none
  * @return none
  */
static bool _get_ds18b20_ack(void)
{
    bool ack, buff;

    buff = EA;
    EA = 0;            ///<disable interrupt
    PIN_18B20 = 0;     ///<make a 500us pulse
    delay_us(500);
    PIN_18B20 = 1;
    delay_us(60);      
    ack = PIN_18B20;   
    while(!PIN_18B20); 
    EA = buff;            

    return ack;
}

/**
  * @brief write data to ds18b20 
  * @param roll[in]: dat "data"
  * @return none
  */
static void _write_ds18b20(uint8_t dat)
{
    uint8_t i;
    bool buff;
    
    buff = EA;
    EA = 0;   
    /* write data from low bool to high bool */
    for (i = 0x01; i != 0; i <<= 1)  
    {
        PIN_18B20 = 0;             
        delay_us(2);
        if ((i & dat) == 0)      
            PIN_18B20 = 0;
        else
            PIN_18B20 = 1;
        delay_us(60);             
        PIN_18B20 = 1;       ///<free ds18b20 bool
    }
    EA = buff;
}

/**
  * @brief read ds18b20 data
  * @param roll[in]: none
  * @return read data
  */
static uint8_t _read_ds18b20(void)
{
    volatile uint8_t dat;
    uint8_t i;
    bool buff;
    
    buff = EA;
    EA = 0;   
    /* write data from low bool to high bool */
    for (i = 0x01; i != 0; i <<= 1)
    {
        PIN_18B20 = 0;          
        delay_us(2);
        PIN_18B20 = 1;         
        delay_us(2);
        if (!PIN_18B20)       ///read the ds18b20 bool
            dat &= ~i;
        else
            dat |= i;
        delay_us(60);        
    }
    EA = buff;   

    return dat;
}

/**
  * @brief start to change temp
  * @param roll[in]: none
  * @return action
  */
bool start_ds18b20(void)
{
    bool ack;
    ack = 0;
    /* reset bus and get a action */
    ack = _get_ds18b20_ack();   
    if (ack == 0)          
    {
        /* change temp */
        _write_ds18b20(0xCC);
        _write_ds18b20(0x44);
    }

    return ~ack;
}

/**
  * @brief read 16 bool temp
  * @param roll[out]: temp "read temp"
  * @return action
  */
bool get_ds18b20Temp(int16_t *temp)
{
    bool ack;
    /* 16 bool temp (12bool(integer):4bool(float):5bool(sign))*/
    uint8_t LSB, MSB;

    /* reset bus and get a action */
    ack = _get_ds18b20_ack();    
    if (ack == 0)           
    {
        /* change temp */
        _write_ds18b20(0xCC);
        _write_ds18b20(0xBE);
        LSB = _read_ds18b20();  ///<read low byte
        MSB = _read_ds18b20();  ///<read high byte
        *temp = ((int16_t)MSB << 8) + LSB;
    }
    return ~ack;             
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
