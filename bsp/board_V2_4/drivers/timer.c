/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file timer.c
  * @version 0.1.0
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-18   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "timer.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
bit digitron_scanFlag = 1;
bit led_runFlag = 1;
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
///<1ms@33.1776MHz
void timer0_init(void)
{
    AUXR |= 0x80;
    TMOD &= 0xF0;
    TMOD |= 0x01;
    TL0 = 0x65;
    TH0 = 0x7E;
    TF0 = 0;
    EA = 1;
    ET0 = 1;
    TR0 = 1;
}

void timer0_interrupt()  interrupt 1
{
    static uint32_t time = 0;
    TL0 = 0x65;
    TH0 = 0x7E;
    time ++;
    digitron_scanFlag = 1;
    if (time == 500)
    {
        time = 0;
        led_runFlag = 1;
        P13 = ~P13;
    }
}

/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
