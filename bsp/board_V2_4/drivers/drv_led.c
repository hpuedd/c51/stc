/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_led.c
  * @version 0.1.0
  * @brief none
  * @details none
  * @warning none
  * @bug none
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-21   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_led.h"
#include <stdio.h>
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
uint8_t P0_BUFF = 0xFF;
sbit LED_EN_RB = P1^1;
/* Private function prototypes -------------------------------------------------------------*/

/* ExPORTed functions ----------------------------------------------------------------------*/
/**
  * @brief Light up LED
  * @param[in] pd: LED data
  * @param[in] pd: LED STATUS
  */
void led_switch(led_list_t ledn,led_status_t status)
{
    if (ledn < LED_MAX)
    {
        if (status == 0)
            P0_BUFF &= ~ (1<<ledn);
        else
            P0_BUFF |= (1 << ledn);
    }
    P0 = P0_BUFF;
}
///Light the small lights in run
void led_run(void)
{
    static uint8_t i = 0;
    
    LED_EN_RB = 1;
    P2 &= (uint8_t) ~(1 << 0);
    led_switch(i++ % 8,0);
    led_switch((i-2) % 8,1);
    LED_EN_RB = 0;
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
