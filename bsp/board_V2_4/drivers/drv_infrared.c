/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_infrared.c
  * @version 0.1.0
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-3-15   |    YC         |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_infrared.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define IR_PIN      P32
#define SEND_PIN    P13
/* Private macro ---------------------------------------------------------------------------*/
#define BASE_FREQUENCY          33117600L
#define TIME_TRANSLATE(us)      (uint16_t)(us * BASE_FREQUENCY / 12 / 1000000)

#define MAX_LIMIT_TIME          TIME_TRANSLATE(10000)
#define START_LONG_TIME         TIME_TRANSLATE(9000)
#define STRAT_SHORT_TIME        TIME_TRANSLATE(4500)
#define LONG_TIME               TIME_TRANSLATE(1690)
#define SHORT_TIME              TIME_TRANSLATE(560)
#define ERROR_TIME              TIME_TRANSLATE(100)
/* Private variables -----------------------------------------------------------------------*/
ir_data_st ir_receive;
ir_data_st ir_send;
/* Private function prototypes -------------------------------------------------------------*/
static uint16_t get_high_time(void);
static uint16_t get_low_time(void);
static void get_infrared_data(ir_data_st* ir_rece);
static void set_time_value(uint16_t us);
/* Exported functions ----------------------------------------------------------------------*/

/**
  * @brief initial infrared
  * @param None
  * @bug None
  */
void infrared_init(void)
{
    AUXR |= 0x80;                   ///<timer0 1T mode
    AUXR &= ~0x40;                  ///<timer1 12T mode

    TH0 = 0xfd;
    TL0 = 0xfd;
    TMOD &= ~0x0f;
    TMOD |= 0x02;                   ///<set timer0 8 bit auto load.
    ET0 = 0;                        ///<close timer interrupt
    TR0 = 1;

    TH1 = 0;
    TL1 = 0;
    TMOD &= ~0xf0;                  ///< 16 bit mode
    TMOD |= 0x10;
    ET1 = 1;                        ///< enable interrupt
    TR1 = 0;

    CCON = 0;                       ///<Initial PCA control register
    CL = 0;                         ///<Reset PCA base timer
    CH = 0;
    CMOD = 0x04;                    ///<Set PCA timer clock source as time0 focus ,Disable PCA timer overflow interrupt
    CCAP0H = 0x00;
    CCAP0L = 0x00;                  ///<PWM0 port output 50% duty cycle square wave
    CCAPM0 = 0x42;                  ///<PCA module-0 work in 8-bit PWM mode and no PCA interrupt
    CR = 1;                         ///<PCA timer stop run

    IT0 = 1;                        ///<falling edge trigger
    EX0 = 1;                        ///<enable external interrupt
    EA = 1;
}

/**
  * @brief Send infrared data
  * @param[in] address: Infrared address
  * @param[in] dat: Infrared data
  * @param[in] ir_sen: Infrared data structure
  * @bug None
  */
void sendInfraredData(uint16_t address, uint8_t dat, ir_data_st* ir_sen)
{
    EX0 = 0;
    TR1 = 1;
    ir_sen->ir_user_code = address >> 8;
    ir_sen->ir_user_rcode = address & 0x00ff;
    ir_sen->ir_data = dat;
    ir_sen->ir_rdata = ~dat;
    ir_sen->ir_flag = 1;
}

/**
  * @brief Send infrared data
  * @param[in] ir_sen: Infrared data structure
  * @bug None
  */
static void send_infrared_data(ir_data_st* ir_sen)
{
    static uint8_t step_count = 0;
    static bit temp_count = 0;
    uint8_t temp_value = 0;

    if (ir_sen->ir_flag == 1)
    {
        if (step_count == 0)
        {
            CCAP0H = 0x40;
            set_time_value(START_LONG_TIME);
            step_count ++;
            return;
        }
        else if (step_count == 1)
        {
            CCAP0H = 0x00;
            set_time_value(STRAT_SHORT_TIME);
            step_count ++;
            return;
        }
        else if (step_count > 1 && step_count < 34)
        {
            if (temp_count == 0)
            {
                CCAP0H = 0x40;
                set_time_value(SHORT_TIME);
                temp_count = 1;
                return;
            }
            else
            {
                CCAP0H = 0x00;
                temp_value = * ((uint8_t*) ir_sen + ((step_count - 2) >> 3));

                if (((temp_value >> ((step_count - 2) % 8)) & 0x01) == 0x01)
                {
                    set_time_value(LONG_TIME);
                }
                else
                {
                    set_time_value(SHORT_TIME);
                }

                step_count ++;
                temp_count = 0;
                return;
            }
        }
        else if (step_count == 34)
        {
            CCAP0H = 0x40;
            set_time_value(SHORT_TIME);
            step_count ++;
            return;
        }
        else
        {
            CCAP0H = 0x00;
            step_count = 0;
            temp_count = 0;
            ir_sen->ir_flag = 0;
            TR1 = 0;
            EX0 = 1;
            return;
        }
    }
}

/**
  * @brief Us delay
  * @param[in] us: delay time
  * @bug None
  */
static void set_time_value(uint16_t us)
{
    uint16_t temp_time = 0;

    temp_time = TIME_TRANSLATE(us);
    TL1 = 256 - temp_time & 0x00ff;
    TH1 = 256 - temp_time >> 8;
}

/**
  * @brief Get hight level time
  * @param[in] None
  * @bug None
  */
static uint16_t get_high_time(void)
{
    uint16_t temp = 0;

    TH1 = 0;
    TL1 = 0;
    TR1 = 1;

    while (IR_PIN)
    {
        temp = TH1 << 8 | TL1;

        if (temp > MAX_LIMIT_TIME)
        {
            break;;
        }
    }

    TR1 = 0;
    return (temp);
}
/**
  * @brief Get low level time
  * @param[in] None
  * @bug None
  */
static uint16_t get_low_time(void)
{
    uint16_t temp = 0;

    TH1 = 0;
    TL1 = 0;
    TR1 = 1;

    while (!IR_PIN)
    {
        temp = TH1 << 8 | TL1;

        if (temp > MAX_LIMIT_TIME)
        {
            break;
        }
    }

    TR1 = 0;
    return (temp);
}
/**
  * @brief Get infrared data
  * @param[out] ir_rece : receive data structure
  * @bug None
  */
static void get_infrared_data(ir_data_st* ir_rece)
{
    uint8_t count1 = 0, count2 = 0;
    uint8_t temp = 0;
    uint16_t time = 0;

    time = get_low_time();

    if ((time > START_LONG_TIME + ERROR_TIME) || (time < START_LONG_TIME - ERROR_TIME))
    {
        IE0 = 0;
        return;
    }

    time = get_high_time();

    if ((time > STRAT_SHORT_TIME + ERROR_TIME) || (time < STRAT_SHORT_TIME - ERROR_TIME))
    {
        IE0 = 0;
        return;
    }

    for (count1 = 0; count1 < 4; count1++)
    {
        for (count2 = 0; count2 < 8; count2++)
        {
            time = get_low_time();

            if ((time > SHORT_TIME + ERROR_TIME) || (time < SHORT_TIME - ERROR_TIME))
            {
                IE0 = 0;
                return;
            }

            time = get_high_time();

            if ((time < SHORT_TIME + ERROR_TIME) && (time > SHORT_TIME - ERROR_TIME))
            {
                temp >>= 1;
            }
            else if ((time < LONG_TIME + ERROR_TIME) && (time > LONG_TIME - ERROR_TIME))
            {
                temp >>= 1;
                temp |= 0x80;
            }
            else
            {
                IE0 = 0;
                return;
            }
        }

        * ((uint8_t*) ir_rece + count1) = temp;
    }

    if (ir_rece->ir_data = ~ir_rece->ir_rdata)
    {
        ir_rece->ir_flag = 1;
    }

    IE0 = 0;
}

/**
  * @brief Infrared delay interrupt
  * @param[in] None
  * @bug None
  */
void infrared_timer_interrupt() interrupt 3
{
    send_infrared_data(&ir_send);
}

/**
  * @brief Infrared receive data interrupt
  * @param[in] None
  * @bug None
  */
void infrared_interrupt() interrupt 0
{
    get_infrared_data(&ir_receive);
}

/**
  * @}
  */
/************************ (C) COPYRIGHT YC *****END OF FILE*********************************/
