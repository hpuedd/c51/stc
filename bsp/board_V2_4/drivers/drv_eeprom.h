/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.h
  * @version 0.1.0	
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed       |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_EEPROM_H__
#define __DRV_EEPROM_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/ 	
/* Exported functions ----------------------------------------------------------------------*/  
uint8_t eeprom_read(uint8_t *buf, uint8_t addr, uint8_t len);
uint8_t eeprom_write(uint8_t *buf, uint8_t addr, uint8_t len);

#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
