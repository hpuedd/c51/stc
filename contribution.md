# 代码贡献指引 #

**_仓库采用简洁的 workflow, 只有一个长期分支 master_**

## 贡献流程 ##

### 团队成员 ###

1. 根据需求， 从 **master** 拉出新分支(在仓库下直接创建新分支，无需fork, ***分支的命名要与开发内容相关***)，不区分功能分支或补丁分支

2. clone 新创建的分支到本地(eg: git clone -b branchName url), 在本地进行开发

3. 在本地开发完后，推送到远程仓库自己的分支，然后向 **master** 发起 merge request （简称MR）

4. MR 既是一个通知， 让其他人注意到你的请求， 又是一种对话机制， 大家一起评审和讨论你的代码。 对话过程中， 你可以不断提交你的代码

5. 评审通过后，你的 MR 被接受，合并进  **master** 后， 一次代码贡献即完成了, 此时应删除你的这个开发分支

### 非团队成员 ###

1. fork 仓库， clone fork 的仓库到本地

2. 在本地完成开发后， push到自己的远程仓库

3. 在 gitlab 向原仓库 **master** 发起 Merge Request

4. 关注 Merge Request 信息，根据评审，修改代码

5. 待代码评审通过后， MR 被合并， 一次代码贡献即完成了

## 文档推荐 ##

[Git 教程](http://www.runoob.com/git/git-tutorial.html)

[Git 教程](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)

[Git 工作流程](http://www.ruanyifeng.com/blog/2015/12/git-workflow.html)

[Git 使用规范流程](http://www.ruanyifeng.com/blog/2015/08/git-use-process.html)

[A guide for programming within version control.](https://github.com/thoughtbot/guides/tree/master/protocol/git)

[keil 添加代码格式化工具 Astyle](https://zhuanlan.zhihu.com/p/23012907)